package main

import (
	"bytes"
	"crypto/sha1"
	"fmt"
	"log"
	"net"
)

const (
	reservedBytesSize int    = 8
	flagsSize         int    = 8
	protocolPstrlen   int    = 19
	protocolPstr      string = "BitTorrent protocol"
)

type (
	Addr struct {
		IP   net.IP
		Port uint16
	}

	Handshake struct {
		Pstrlen  byte
		Pstr     string
		Reserved [reservedBytesSize]byte
		InfoHash [sha1.Size]byte
		PeerId   [peerIdMaxSize]byte
	}

	HandshakeResponse struct {
		Flags    [flagsSize]byte
		InfoHash [sha1.Size]byte
		PeerId   [peerIdMaxSize]byte
	}
)

func (a Addr) TCPAddr() *net.TCPAddr {
	return &net.TCPAddr{a.IP, int(a.Port), ""}
}

func (a Addr) UDPAddr() *net.UDPAddr {
	return &net.UDPAddr{a.IP, int(a.Port), ""}
}

func (a Addr) String() string {
	return fmt.Sprintf("%s:%d", a.IP.String(), a.Port)
}

func NewHandshake(infoHash [sha1.Size]byte, peerId [peerIdMaxSize]byte) *Handshake {
	return &Handshake{Pstrlen: byte(protocolPstrlen),
		Pstr:     protocolPstr,
		InfoHash: infoHash,
		PeerId:   peerId}
}

func (h Handshake) Bytes() []byte {
	var buf bytes.Buffer
	buf.WriteByte(h.Pstrlen)
	buf.WriteString(h.Pstr)
	buf.Write(h.Reserved[:])
	buf.Write(h.InfoHash[:])
	buf.Write(h.PeerId[:])
	return buf.Bytes()
}

func DecodeHandshakeResponse(response []byte) (resp HandshakeResponse) {
	if response[0] != 19 {
		log.Fatal("Handshake response did not begin with 19 or was not the correct length of 68")
	}
	if string(response[1:20]) != protocolPstr {
		log.Fatal("Protocol must be BitTorrent protocol")
	}
	copy(resp.Flags[:], response[20:28])
	copy(resp.InfoHash[:], response[28:48])
	copy(resp.PeerId[:], response[48:68])
	return resp
}
