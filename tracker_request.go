package main

import (
	"crypto/sha1"
	"encoding/binary"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"net/http"
	"net/url"
	"time"

	"github.com/mvanbrummen/go-bencoder"
)

type (
	Event int

	TrackerRequest struct {
		InfoHash   [sha1.Size]byte
		PeerId     [peerIdMaxSize]byte
		NoPeerId   rune
		IP         string
		Port       uint16
		Uploaded   uint64
		Downloaded uint64
		Left       uint64
		Event      Event
		Compact    rune
	}

	TrackerResponse struct {
		FailureReason  string
		WarningMessage string
		Interval       int
		MinInterval    int
		TrackerId      string
		Complete       int
		Incomplete     int
		Peers          []Peer
	}
)

const (
	peerIdMaxSize       int    = 20
	peerIdClient        string = "-CU"
	peerIdClientVersion string = "0.0.1"
	randomChars         string = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
)

var (
	events = []string{"started", "completed", "stopped"}
)

const (
	started Event = iota
	completed
	stopped
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func (e Event) String() string {
	return events[e-1]
}

func NewTrackerRequest(infoHash [sha1.Size]byte, length uint64) *TrackerRequest {
	return &TrackerRequest{InfoHash: infoHash,
		PeerId:     GenerateRandPeerId(),
		NoPeerId:   '0',
		Port:       6881,
		Uploaded:   0,
		Downloaded: 0,
		Left:       length,
		Event:      started,
		Compact:    '1',
	}
}

func GenerateRandPeerId() (peerId [peerIdMaxSize]byte) {
	prepend := []byte(fmt.Sprintf("%s%s", peerIdClient, peerIdClientVersion))
	randCharSlice := []byte(randomChars)
	for i := 0; i < peerIdMaxSize; i++ {
		if i < len(prepend) {
			peerId[i] = prepend[i]
		} else {
			peerId[i] = randCharSlice[rand.Intn(len(randomChars))]
		}
	}
	return peerId
}

func (tr TrackerRequest) QueryString() string {
	rawurl := fmt.Sprintf("?info_hash=%s&peer_id=%s&port=%d&downloaded=%d&left=%d&event=%s&no_peer_id=%c&compact=%c&uploaded=%d",
		url.QueryEscape(fmt.Sprintf("%s", tr.InfoHash)),
		url.QueryEscape(fmt.Sprintf("%s", string(tr.PeerId[:]))),
		tr.Port,
		tr.Downloaded,
		tr.Left,
		"started", // TODO fix this to set enum val
		tr.NoPeerId,
		tr.Compact,
		tr.Uploaded)
	return rawurl
}

func (tr TrackerRequest) Connect(announce []byte) *TrackerResponse {
	var peerList []Peer
	url := fmt.Sprintf("%s%s", announce, tr.QueryString())
	resp, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	} else {
		defer resp.Body.Close()
		resp := ParseTrackerResponse(resp.Body)
		rawPeers, _ := resp["peers"].([]byte)
		peerList = GetPeersList(rawPeers)
	}
	return &TrackerResponse{Peers: peerList}
}

func ParseTrackerResponse(r io.Reader) (resp map[string]interface{}) {
	body, err := ioutil.ReadAll(r)
	if err != nil {
		log.Fatal(err)
	}
	decodedBody, err := bencode.Unmarshal([]byte(body))
	if err != nil {
		log.Fatal(err)
	}
	resp, ok := decodedBody.(map[string]interface{})
	failure, ok := resp["failure reason"]
	if ok {
		failuremsg, _ := failure.([]byte)
		log.Fatal(fmt.Sprintf("%s", failuremsg))
	}
	return resp
}

func GetPeersList(peers []byte) (peerList []Peer) {
	offset := 0
	for offset < len(peers) {
		peerIp := net.IPv4(peers[offset], peers[offset+1], peers[offset+2], peers[offset+3])
		peerPort := binary.BigEndian.Uint16(peers[offset+4 : offset+6])
		peer := Peer{address: Addr{peerIp, peerPort}}
		peerList = append(peerList, peer)
		offset += 6
	}
	return peerList
}
