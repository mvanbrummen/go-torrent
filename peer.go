package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
)

type Peer struct {
	PeerId      [peerIdMaxSize]byte
	isConnected bool
	address     Addr
}

func (p Peer) Connect(handshake *Handshake) (HandshakeResponse, error) {
	log.Printf("\nDialing %s ...\n", p.Address().String())
	conn, err := net.Dial("tcp", p.Address().String())
	if err != nil {
		return HandshakeResponse{}, err
	}
	defer conn.Close()
	log.Print("Writing handshake\n")
	conn.Write(handshake.Bytes())
	log.Print("Done writing handshake\n")
	for {
		message, err := bufio.NewReader(conn).ReadBytes('\n')
		if err != nil {
			return HandshakeResponse{}, err
		}
		if len(message) > 0 {
			fmt.Printf("Message from server: %s\n", message)
			resp := DecodeHandshakeResponse(message)
			fmt.Printf("Infohash is %s peerid is %s flags is %s\n", resp.InfoHash, resp.PeerId, resp.Flags)
			return resp, nil
		}
	}
}

func (p Peer) IsConnected() bool {
	return p.isConnected
}

func (p Peer) Address() Addr {
	return p.address
}
