package main

import (
	_ "flag"
	"fmt"
	"log"
	"os"
)

func main() {
	if len(os.Args) == 1 {
		log.Fatal("First argument must be a path to a .torrent file")
	}
	// read metadata
	torrentFile := ReadTorrentFile(os.Args[1])

	// build tracker request
	trackerRequest := NewTrackerRequest(torrentFile.InfoHash(), uint64(torrentFile.Length()))
	trackerResponse := trackerRequest.Connect(torrentFile.Announce())

	// tcp connection to peer
	handshake := NewHandshake(torrentFile.InfoHash(), trackerRequest.PeerId)

	messages := make(chan HandshakeResponse, len(trackerResponse.Peers))
	for _, peer := range trackerResponse.Peers {
		go func(peer Peer) {
			if resp, err := peer.Connect(handshake); err != nil {
				messages <- resp
			}
		}(peer)
	}
	for i := 0; i < len(trackerResponse.Peers); i++ {
		fmt.Printf("messages is %+v\n", <-messages)
	}
}
