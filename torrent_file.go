package main

import (
	"crypto/sha1"
	"io/ioutil"
	"log"

	"github.com/mvanbrummen/go-bencoder"
)

type TorrentFile struct {
	MetaData map[string]interface{}
}

func ReadTorrentFile(file string) *TorrentFile {
	b, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	node, err := bencode.Unmarshal(b)
	if err != nil {
		log.Fatal(err)
	}
	dict, ok := node.(map[string]interface{})
	if !ok {
		log.Fatal("Not a dictionary")
	}
	return &TorrentFile{dict}
}

func (t *TorrentFile) Announce() []byte {
	announce, ok := t.MetaData["announce"].([]byte)
	if !ok {
		log.Fatal("Not a dicionary")
	}
	return announce
}

func (t *TorrentFile) InfoDictionary() map[string]interface{} {
	info, ok := t.MetaData["info"].(map[string]interface{})
	if !ok {
		log.Fatal("Not a dictionary")
	}
	return info
}

func (t *TorrentFile) InfoHash() [sha1.Size]byte {
	info, err := bencode.Marshal(t.InfoDictionary())
	if err != nil {
		log.Fatal(err)
	}
	return sha1.Sum(info)
}

func (t *TorrentFile) Length() int64 {
	length, ok := t.InfoDictionary()["length"].(int64)
	if !ok {
		log.Fatal("Length not an int")
	}
	return length
}
